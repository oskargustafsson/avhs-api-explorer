var vent = _.extend({}, Backbone.Events);
var baseUrl = 'http://avhs-dev.axis.com'; // replace with location.origin


//// COLLECTIONS

var APICollection = Backbone.Collection.extend({

  initialize: function (models, options) {
    this.parentModel = options.parentModel;
  },

  fetch: function (options) {
    options = options || {};
    options.dataType = 'html';
    return Backbone.Collection.prototype.fetch.call(this, options);
  }

});



var ControllerCollection = APICollection.extend({

  url: function () {
    return baseUrl + '/documentation.php?show=all&' + $.param(this.parentModel.pick('u', 'p'));
  },

  parse: function (response) {
    return $($.parseHTML(response)).find('.controller a').map(function (i, el) {
      return { name: $(el).text() };
    }).get();
  }

});



var ActionCollection = APICollection.extend({

  url: function () {
    return this.parentModel.collection.url() + '&' + $.param({ controller: this.parentModel.get('name') });
  },

  parse: function (response) {
    var actions = [];
    $($.parseHTML(response)).find('.action > a').each(function (i, el) {
      actions.push({ name: $(el).text(), version: 1 });
      $(el).parent().find('.version-action').each(function (i, versionEl) {
        actions.push({ name: $(el).text(), version: +$(versionEl).text().match(/\d+/) });
      });
    });
    return actions;
  }

});



var ParamCollection = APICollection.extend({

  url: function () {
    return this.parentModel.collection.url() + '&' + $.param({
      action: this.parentModel.get('name'),
      version: this.parentModel.get('version')
    });
  },

  parse: function (response) {
    return $($.parseHTML(response)).find('.request .doc-param').map(function (i, el) {
      return {
        name: $(el).find('h5').text().match(/[\w\[\]]+/),
        isArray: !!$(el).find('h5').text().match(/\[\]/),
        mandatory: !!$(el).find('h5').text().match(/Mandatory/),
        valid: $.trim($(el).find('p b:contains("Valid")').parent().contents().filter(function () {
          return this.nodeType === Node.TEXT_NODE;
        }).text())
      };
    }).get();
  }

});



//// TRAITS

function addTraits(object) {
  var subject = object.prototype || object;
  var traits = _.rest(arguments);
  _.each(traits, function (trait) {
    _.each(_.intersection(_.functions(subject), _.functions(trait)), function (attr) {
      var originalFunction = subject[attr];
      subject[attr] = function () {
        var ret = originalFunction.apply(this, arguments);
        trait[attr].apply(this, arguments);
        return ret;
      }
    });
    _.defaults(subject, trait);
  });
  return object;
}



var ClassNameFromName = {

  className: function () { return this.name + '-view'; }

};



var TemplateFromName = {

  initialize: function () {
    this.template = _.template($('.' + this.name + '-view-template').html());
  }

};



var ModelChangeEventEmitter = {

  initialize: function () {
    this.listenTo(this.model, 'change',
        _.bind(vent.trigger, vent, this.name + '-changed', this.model));
  },

};



var Selectable = {

  initialize: function () {
    this.state = new Backbone.Model({ selected: false });
    this.listenTo(vent, this.name + '-selected', this.onSelected);
    this.listenTo(this.state, 'change:selected', this.onSelectedChanged);
  },

  onSelected: function (selectedView) {
    this.state.set('selected', this === selectedView);
  },

  onSelectedChanged: function (state, selected) {
    this.$el.toggleClass('selected', selected);
  },

  setFocus: function () {
    vent.trigger(this.name + '-selected', this);
  },

};



var Parent = {

  initialize: function () {
    this.childCollection = new this.ChildCollection([], { parentModel: this.model });
    this.listenTo(this.childCollection, 'add', this.onChildAdded);
    this.listenTo(this.childCollection, 'sync', this.onAllChildrenAdded);
    this.addDelay = 0;  // For prettier presentation
  },

  onChildAdded: function (model) {
    // Todo: turn this entire thing into an event trigger and listen to it from AppView
    $container = $('main').find('.' + this.childName + '.container');
    $container.addClass('visible');
    _.delay(function (self) {
      var childView = new self.ChildView({ model: model });
      self.state && childView.listenTo(
          self.state, 'change:selected', childView.onParentSelectedChanged);
      $container.append(childView.render().el);
    }, 100 * Math.sqrt(this.addDelay++) | 0, this);
  },

  onAllChildrenAdded: function () { this.addDelay = 0; },

  onSelectedChanged: function (selected) {
    if (!selected) { return; }
    this.childCollection.length || this.childCollection.fetch();
  },

};



var Child = {

  onParentSelectedChanged: function (parentState, selected) {
    this.$el.toggle(!!selected);
  }

};


//// VIEWS

var URLView = addTraits(Backbone.View.extend({

  name: 'url',

  initialize: function () {
    this.listenTo(vent, 'param-changed', this.onParamChanged);
    this.listenTo(vent, 'user-changed', this.onUserChanged);
    this.listenTo(vent, 'controller-selected', this.onControllerSelected);
    this.listenTo(vent, 'action-selected', this.onActionSelected);
    this.listenTo(this.model, 'change', this.onModelChanged);
  },

  onModelChanged: function (model) {
    var controller = model.get('controller');
    var action = model.get('action');
    var query = _.extend({ api: 'PHP' }, model.get('params'), model.get('user'));

    var url = '';
    if (controller) {
      url += baseUrl + '/' + controller + '.php';
      if (action) {
        action.version > 1 && (query.v = action.version);
        url += '?' + controller + '_action=' + action.name + '&' + decodeURIComponent($.param(query));
      }
    }

    this.$('.url').text(url).attr('href', url);
  },

  onUserChanged: function (userModel) {
    this.model.set('user', userModel.toJSON());
  },

  onControllerSelected: function (controllerView) {
    this.model.set('controller', controllerView.model.get('name'));
  },

  onActionSelected: function (actionView) {
    if (actionView) {
      this.model.set('action', actionView.model.pick('name', 'version'));
    } else {
      this.model.unset('action');
    }
  },

  onParamChanged: function (paramModel) {
    var params = paramModel.collection.filter(function (parameter) {
      return !!parameter.get('value');
    }).reduce(function (params, param) {
      params[param.get('name')] = param.get('value');
      return params;
    }, {});
    this.model.set('params', params);
  },

  render: function () {
    this.$el.html(this.template());
    return this;
  }

}), TemplateFromName, ClassNameFromName);



var ModelBoundView = addTraits(Backbone.View.extend({

  initialize: function (options) {
    this.modelBinder = new Backbone.ModelBinder();
  },

  render: function (bindings) {
    this.$el.html(this.template(this.model.toJSON()));
    this.modelBinder.bind(this.model, this.el, bindings);
    return this;
  }

}), TemplateFromName, ClassNameFromName);



var UserView = addTraits(ModelBoundView.extend({

  name: 'user',

}), ModelChangeEventEmitter);



var ParamView = addTraits(ModelBoundView.extend({

  name: 'param',

  parseValue: function (direction, value, attribute, model) {
    if (direction === 'ViewToModel' && model.get('isArray')) {
      return value.split(',').map($.trim);
    }
    return $.trim(value);
  },

  parseMandatory: function (direction, value) {
    if (direction === 'ModelToView') {
      return value ? '*' : '';
    } else { throw new Error(); }
  },

  render: function () {
    return ModelBoundView.prototype.render.call(this, {
      name: '[name=name]',
      value: { selector: '[name=value]', converter: this.parseValue },
      mandatory: { selector: '[name=mandatory]', converter: this.parseMandatory }
    });
  },

}), ModelChangeEventEmitter, Child);



var ActionView = addTraits(ModelBoundView.extend({

  name: 'action',
  childName: 'param',
  ChildCollection: ParamCollection,
  ChildView: ParamView,

  events: {
    'click': 'setFocus'
  },

  onSelected: function (selectedView) {
    Selectable.onSelected.apply(this, arguments);
    this === selectedView && vent.trigger('param-changed', { collection: this.childCollection }); // force repaint
  },

  parseVersion: function (direction, value) {
    if (direction === 'ModelToView') {
      return value > 1 ? 'v' + value : '';
    } else { throw new Error(); }
  },

  render: function () {
    return ModelBoundView.prototype.render.call(this, {
      name: '[name=name]',
      version: { selector: '[name=version]', converter: this.parseVersion }
    });
  },

}), Selectable, Parent, Child);



var ControllerView = addTraits(ModelBoundView.extend({

  name: 'controller',
  childName: 'action',
  ChildCollection: ActionCollection,
  ChildView: ActionView,

  events: {
    'click': 'setFocus'
  },

  setFocus: function () {
    vent.trigger('action-selected');  // Clear the selected action
    Selectable.setFocus.apply(this, arguments);
  },

}), Selectable, Parent);



var AppView = addTraits(Backbone.View.extend({

  el: $('.avhs-api-browser'),
  childName: 'controller',
  ChildCollection: ControllerCollection,
  ChildView: ControllerView,

  initialize: function () {
    this.userView = new UserView({ model: new Backbone.Model() });
    this.urlView = new URLView({ model: new Backbone.Model() });
    this.model = this.userView.model;
  },

  events: {
    'click .get-controllers': 'fetchControllers',
    'submit form': 'fetchControllers',
  },

  fetchControllers: function (e) {
    e && e.preventDefault();
    this.childCollection.length || this.childCollection.fetch();
  },

  render: function () {
    this.$('header').append(this.userView.render().el);
    this.$('footer').append(this.urlView.render().el);
    return this;
  }

}), Parent);



var app = new AppView();
app.render();
